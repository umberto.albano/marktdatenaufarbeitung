import json
import pandas as pd
import csv

#input_file = "./20190618091118000194_6482991498337965.json"
#output_file_json = "./marktdaten_example.json"
#output_file = "./20190618091118000194_6482991498337965.csv"

input_file = "./example.json"
output_file = "./orders_example.csv"
output_file_json = "./orders_example.json"
verbose = True # (dis)able logging


def log(msg:str):

    """
    Print messages, if verbose is true
    """

    if verbose:
        print(msg)

def orders_to_azure(orders:list):
    pass

def orders_to_json(file: str, output: list):

    """
    Write all Orders to a json file
    """

    with open(file, 'w') as f:
        json.dump(output, f)

def orders_to_csv(file: str, output: list):

    """
    Write all Orders to a csv file
    """
    keys = output[0].keys()

    with open(file, 'w', newline='\n')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(output)


def parse_payload(payload):
    """
    Parse the payload to a list of orders and acceptions of orders
    """
    output = []
    accepted_orders = {}

    for i in payload:

        orderbook_list = i["ordrbookList"]
        for order_book in orderbook_list["ordrBook"]:

            # There are to differentlists for Sell and Buy orders
            buy_orders = order_book["buyOrdrList"]
            sell_orders = order_book["sellOrdrList"]

            if buy_orders:

                log("Read "+ str(len(buy_orders)) + " Buy Orders")

                for order in buy_orders["ordrBookEntry"]:

                    id = int(order["ordrId"]) 
                    del order["ordrId"]
                    order["id"] = str(id)

                    order["typ"] = "buy"
                    order["accepted"] = False
                    order["product"] = 1611242100000 # Dummyvalur

                    # If an Order have a qty of 0, this not a new Order
                    # This is the acception of a given Order, with the same ID
                    if order["qty"] != 0:
                        log(json.dumps(order))
                        output.append(order)
                    else:
                        
                        accepted_orders[id] = order
            else:
                log("No Buy Orders")

            if sell_orders:

                log("Read "+ str(len(sell_orders)) + " Sell Orders")

                for order in sell_orders["ordrBookEntry"]:

                    id = int(order["ordrId"]) 
                    del order["ordrId"]
                    order["id"] = str(id)

                    order["typ"] = "sell"
                    order["accepted"] = False
                    order["product"] = 1611242100000 # Dummy Value

                    if order["ordrType"]:
                        print(order["ordrType"])

                    if order["qty"] != 0:
                        log(json.dumps(order))
                        output.append(order)
                    else:

                        accepted_orders[id] = order
            else:
                log("No Sell Orders")

    return output,accepted_orders


def match_orders(orders : list, accepted_orders:dict):

    """
    Find matching Orderlist with list off accepted Orders.
    If a acception is find, the Boolean changes to true
    """
    for order in orders:

        id = int(order["id"])

        if id in accepted_orders:
            accepted_order = accepted_orders[id]

            log("Found matched Order with id:"+ str(id))

            order["accepted"] = True
            #order["accepted_px"] = accepted_order["px"]
            #order["exception_time"] = accepted_order["ordrEntryTime"]

    return orders

print("Parse file",input_file,"to",output_file)

try:
    data = pd.read_json(input_file)

    payload = data['payload']
    orders, excpeted_orders = parse_payload(payload)
    orders = match_orders(orders,excpeted_orders)
    orders_to_csv(output_file,orders)
    orders_to_json(output_file_json,orders)
    n_orders = len(orders)
    print("Parse", str(n_orders), "successfully")

except Exception as e:
    print("Exception while parsing:")
    print(e)




